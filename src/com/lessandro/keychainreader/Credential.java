package com.lessandro.keychainreader;

public class Credential {

	public byte[] key;
	public byte[] iv;
	public byte[] ciphertext;

	public String name;
	public String account;
	public String where;
	public String comments;
	public String password;

}
