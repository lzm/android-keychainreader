package com.lessandro.keychainreader;

import java.io.File;
import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.Key;
import java.security.spec.KeySpec;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class Keychain {

	private final byte[] buffer;
	private byte[] wrapping_key;
	public final Map<ByteBuffer, Credential> credentials;

	public Keychain(File file, String password) throws Exception {
		credentials = new HashMap<ByteBuffer, Credential>();
		buffer = readFile(file);

		dump_wrapping_key(password);
		dump_keychain();
		decrypt_credentials();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (Credential cred : credentials.values()) {
			sb.append("name: " + cred.name + "\n");
			sb.append("account: " + cred.account + "\n");
			sb.append("where: " + cred.where + "\n");
			sb.append("password: " + cred.password + "\n");
			sb.append("comments: " + cred.comments + "\n");
			sb.append("\n");
		}
		return sb.toString();
	}

	private byte[] readFile(File file) throws Exception {
		byte[] contents = new byte[(int) file.length()];
		FileInputStream fileInputStream = new FileInputStream(file);
		fileInputStream.read(contents);
		return contents;
	}

	private byte[] decrypt_3des(byte[] ciphertext, byte[] key, byte[] iv) throws Exception {
		Key keySpec = new SecretKeySpec(key, "DESede");
		IvParameterSpec ivSpec = new IvParameterSpec(iv);
		Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
		cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);
		return cipher.doFinal(ciphertext);
	}

	private byte[] derive_key(String password, byte[] salt) throws Exception {
		SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 1000, 192);
		return keyFactory.generateSecret(spec).getEncoded();
	}

	private Credential find_or_create_credentials(byte[] label) {
		ByteBuffer key = ByteBuffer.wrap(label);
		if (!credentials.containsKey(key))
			credentials.put(key, new Credential());

		return credentials.get(key);
	}

	private void decrypt_credentials() throws Exception {
		for (Credential cred : credentials.values()) {
			if (cred.ciphertext == null)
				continue;
			if (cred.key == null)
				continue;
			byte[] password = decrypt_3des(cred.ciphertext, cred.key, cred.iv);
			cred.password = new String(password, "UTF-8");
		}
	}

	private void dump_wrapping_key(String password) throws Exception {
		int offset;
		int sz = buffer.length;

		for (offset = sz - 4; offset > 0; offset -= 4)
			if (atom32(offset) == 0xfade0711)
				break;

		if (offset <= 0)
			return;

		byte[] salt = memcpy(offset + 44, 20);
		byte[] master_key = derive_key(password, salt);
		byte[] iv = memcpy(offset + 64, 8);
		int ciphertext_offset = atom32(offset + 8);
		byte[] ciphertext = memcpy(offset + ciphertext_offset, 48);
		byte[] padded_key = decrypt_3des(ciphertext, master_key, iv);

		wrapping_key = Arrays.copyOfRange(padded_key, 0, 24);
	}

	private void dump_keychain() throws Exception {
		if (strncmp(buffer, "kych".getBytes(), 4))
			return;

		int schema = atom32(12);

		// Traverse each table
		int table_count = atom32(schema + 4);
		for (int i = 0; i < table_count; ++i) {
			int table_offset = atom32(schema + 8 + i * 4);
			int table = schema + table_offset;

			// Traverse each record
			int record_count = atom32(table + 8);
			for (int j = 0; j < record_count; ++j) {
				int record_offset = atom32(table + 28 + j * 4);
				int record = table + record_offset;

				// Calculate the start of the data section
				int record_sz = atom32(record + 0);
				int data_sz = atom32(record + 16);
				int data_offset = 24;
				if (record_sz > 24 + data_sz) {
					int first_attribute_offset = atom32(record + 24) & 0xfffffffe;
					data_offset = first_attribute_offset - data_sz;
				}
				int data = record + data_offset;

				int magic = atom32(data + 0);

				if (magic == 0xfade0711) {
					dump_key_blob(data);
				} else if (magic == 0x73736770) {
					dump_credentials_data(record);
				}
			}
		}
	}

	private void dump_key_blob(int blob) throws Exception {
		int ciphertext_offset = atom32(blob + 8);
		int blob_len = atom32(blob + 12);
		byte[] iv = memcpy(blob + 16, 8);

		// The label is actually an attribute after the KeyBlob
		byte[] label = memcpy(blob + blob_len + 8, 20);

		if (strncmp(label, "ssgp".getBytes(), 4))
			return;

		int ciphertext_len = blob_len - ciphertext_offset;
		if (ciphertext_len != 48)
			return;

		// Decrypt the obfuscation IV layer
		byte[] obfuscationIv = new byte[] { 0x4a, (byte) 0xdd, (byte) 0xa2, 0x2c, 0x79, (byte) 0xe8, 0x21, 0x05 };
		byte[] ciphertext = memcpy(blob + ciphertext_offset, 48);
		byte[] tmp = decrypt_3des(ciphertext, wrapping_key, obfuscationIv);

		// Reverse the fist 32 bytes
		byte[] reverse = new byte[32];
		for (int i = 0; i < 32; ++i)
			reverse[31 - i] = tmp[i];

		// Decrypt the real IV layer
		tmp = decrypt_3des(reverse, wrapping_key, iv);
		if (tmp.length != 28)
			return;

		Credential cred = find_or_create_credentials(label);
		// Discard the first 4 bytes
		cred.key = memcpy(tmp, 4, 24);
	}

	private void dump_credentials_data(int record) throws Exception {
		int record_sz = atom32(record + 0);
		int data_sz = atom32(record + 16);

		// No attributes?
		if (record_sz == 24 + data_sz)
			return;

		int first_attribute_offset = atom32(record + 24) & 0xfffffffe;
		int data_offset = first_attribute_offset - data_sz;
		int attribute_count = (data_offset - 24) / 4;

		if (attribute_count < 16)
			return;

		int data = record + data_offset;

		int ciphertext_len = data_sz - 20 - 8;
		if (ciphertext_len < 8)
			return;
		if (ciphertext_len % 8 != 0)
			return;

		byte[] label = memcpy(data + 0, 20);

		Credential cred = find_or_create_credentials(label);

		cred.iv = memcpy(data + 20, 8);
		cred.ciphertext = memcpy(data + 28, ciphertext_len);

		cred.name = read_attribute(record, 7);
		cred.account = read_attribute(record, 13);
		cred.where = read_attribute(record, 14);
		cred.comments = read_attribute(record, 3);
	}

	private String read_attribute(int record, int attr_num) throws Exception {
		int attribute_offset = atom32(record + 24 + attr_num * 4) & 0xfffffffe;
		int attribute = record + attribute_offset;
		int len = atom32(attribute + 0);

		// Attributes with ridiculous lengths probably aren't strings
		if (len == 0 || len > 1024)
			return null;

		byte[] data = memcpy(attribute + 4, len);
		if (data[0] == 0)
			return "";
		return new String(data, "UTF-8");
	}

	private int atom32(int offset) {
		ByteBuffer bb = ByteBuffer.wrap(buffer, offset, 4);
		bb.order(ByteOrder.BIG_ENDIAN);
		return bb.getInt();
	}

	private byte[] memcpy(int offset, int length) {
		return memcpy(buffer, offset, length);
	}

	private byte[] memcpy(byte[] array, int offset, int length) {
		return Arrays.copyOfRange(array, offset, offset + length);
	}

	private boolean strncmp(byte[] array1, byte[] array2, int length) {
		for (int i = 0; i < length; i++) {
			if (array1[i] != array2[i])
				return true;
		}
		return false;
	}

}
