package com.lessandro.keychainreader;

import java.io.File;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.widget.TextView;

public class MainActivity extends Activity {

	String keychain_path = "path/to.keychain";
	String keychain_password = "keychain password";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		TextView tv = (TextView) findViewById(R.id.textview);
		tv.setMovementMethod(new ScrollingMovementMethod());

		try {
			File dir = Environment.getExternalStorageDirectory();
			File file = new File(dir, keychain_path);

			Keychain keychain = new Keychain(file, keychain_password);
			tv.setText(keychain.toString());
		} catch (Exception e) {
			tv.setText(e.toString());
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
